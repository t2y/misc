//go:build windows
// +build windows

package main

import (
	"bufio"
	"fmt"

	"github.com/Microsoft/go-winio"
)

const pipeName = `\\.\pipe\my-pipe`

func receive() {
	l, err := winio.ListenPipe(pipeName, nil)
	if err != nil {
		fmt.Println("ListenPipe error: ", err)
		return
	}
	defer l.Close()

	c, err := l.Accept()
	if err != nil {
		fmt.Println("Accept error: ", err)
	}
	defer c.Close()
	fmt.Println("accepted")

	r := bufio.NewReader(c)
	s, err := r.ReadString('\n')
	if err != nil {
		fmt.Println("ReadString error: ", err)
	}
	fmt.Printf("received: %s", s)
}

func main() {
	receive()
}
