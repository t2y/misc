package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"golang.org/x/oauth2/google"
	admin "google.golang.org/api/admin/directory/v1"
	"google.golang.org/api/googleapi"
	"google.golang.org/api/option"
)

var (
	clientEmail = os.Getenv("CLIENT_EMAIL")
	privateKey  = strings.ReplaceAll(os.Getenv("PRIVATE_KEY"), "\\n", "\n")
	subject     = os.Getenv("SUBJECT")

	primaryEmail = os.Getenv("PRIMARY_EMAIL")
)

func newService() (*admin.Service, error) {
	ctx := context.Background()
	credentials, err := json.Marshal(map[string]string{
		"client_email": clientEmail,
		"private_key":  privateKey,
		"type":         "service_account",
	})
	if err != nil {
		return nil, err
	}
	config, err := google.JWTConfigFromJSON(
		credentials,
		admin.AdminDirectoryUserScope,
		admin.AdminDirectoryGroupScope,
	)
	if err != nil {
		return nil, fmt.Errorf("create jwt config error: %w", err)
	}
	config.Subject = subject
	ts := config.TokenSource(ctx)
	srv, err := admin.NewService(ctx, option.WithTokenSource(ts))
	if err != nil {
		return nil, fmt.Errorf("create new service error: %w", err)
	}
	return srv, nil
}

func insertUser(srv *admin.Service, pEmail string) error {
	names := strings.Split(strings.Split(pEmail, "@")[0], "-")
	au := admin.User{
		PrimaryEmail: pEmail,
		Name: &admin.UserName{
			GivenName:  names[0],
			FamilyName: names[1],
		},
		Password: "password",
	}
	created, err := srv.Users.Insert(&au).Do()
	if err != nil {
		if ge, ok := err.(*googleapi.Error); ok {
			fmt.Printf("%+v\n", ge)
		}
		return err
	}
	fmt.Printf("created: %+v\n", created)
	return nil
}

func listUsers(srv *admin.Service, domain string) error {
	res, err := srv.Users.List().Domain(domain).
		MaxResults(5).OrderBy("email").Do()
	if err != nil {
		if ge, ok := err.(*googleapi.Error); ok {
			fmt.Printf("%+v\n", ge)
		}
		return err
	}
	for _, u := range res.Users {
		fmt.Printf("%+v\n", u.Name)
	}
	return nil
}

func insertAliasAndDelete(srv *admin.Service, pEmail string) error {
	var err error
	var alias *admin.Alias
	aliasParam := &admin.Alias{
		Alias: "my-alias1@" + strings.Split(pEmail, "@")[1],
	}
	for i := 0; i < 200; i++ {
		alias, err = srv.Users.Aliases.Insert(pEmail, aliasParam).Do()
		if err != nil {
			if ge, ok := err.(*googleapi.Error); ok {
				fmt.Printf("i: %+v\n", i)
				fmt.Printf("Code: %+v\n", ge.Code)
				fmt.Printf("Message: %+v\n", ge.Message)
				fmt.Printf("Details: %+v\n", ge.Details)
			} else {
				log.Print(err.Error())
			}
		}
		if alias != nil {
			fmt.Printf("insert-aliased: %+v\n", alias)
			break
		}
		<-time.After(100 * time.Millisecond)
	}

	if alias != nil {
		if err := srv.Users.Delete(pEmail).Do(); err != nil {
			if ge, ok := err.(*googleapi.Error); ok {
				fmt.Printf("%+v\n", ge)
			}
			log.Fatal(err)
		}
		fmt.Println("deleted after insert alias")
	}
	return nil
}

func main() {
	srv, err := newService()
	if err != nil {
		log.Fatal(err)
	}

	if err := insertUser(srv, primaryEmail); err != nil {
		log.Fatal(err)
	}

	var user *admin.User
	for i := 0; i < 200; i++ {
		user, err = srv.Users.Get(primaryEmail).Do()
		if err != nil {
			if ge, ok := err.(*googleapi.Error); ok {
				fmt.Printf("i: %+v\n", i)
				fmt.Printf("Code: %+v\n", ge.Code)
				fmt.Printf("Message: %+v\n", ge.Message)
				fmt.Printf("Details: %+v\n", ge.Details)
			} else {
				log.Print(err.Error())
			}
		}
		if user != nil {
			fmt.Printf("get: %+v\n", user)
			break
		}
		<-time.After(100 * time.Millisecond)
	}

	if user != nil {
		if err := srv.Users.Delete(primaryEmail).Do(); err != nil {
			if ge, ok := err.(*googleapi.Error); ok {
				fmt.Printf("%+v\n", ge)
			}
			log.Fatal(err)
		}
		fmt.Println("deleted")
	}

	if err := listUsers(srv, strings.Split(primaryEmail, "@")[1]); err != nil {
		log.Fatal(err)
	}
}
