package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/policy"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	graphgroups "github.com/microsoftgraph/msgraph-sdk-go/groups"
)

var (
	clientID     = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")
	tenantID     = os.Getenv("TENANT_ID")

	defaultScopes = []string{
		"https://graph.microsoft.com/.default",
	}
)

func main() {
	options := &azidentity.ClientSecretCredentialOptions{
		ClientOptions: azcore.ClientOptions{
			Retry: policy.RetryOptions{
				MaxRetries:    3,
				TryTimeout:    10 * time.Second,
				RetryDelay:    20 * time.Second,
				MaxRetryDelay: 80 * time.Second,
			},
		},
	}
	cred, err := azidentity.NewClientSecretCredential(
		tenantID, clientID, clientSecret, options,
	)
	client, err := msgraphsdk.NewGraphServiceClientWithCredentials(cred, defaultScopes)
	if err != nil {
		log.Fatal(err)
	}
	ctx := context.Background()
	selects := []string{
		"description",
		"displayName",
		"id",
		"mail",
		"securityIdentifier",
	}
	groupsConf := &graphgroups.GroupsRequestBuilderGetRequestConfiguration{
		QueryParameters: &graphgroups.GroupsRequestBuilderGetQueryParameters{
			Select: selects,
		},
	}
	r, err := client.Groups().Get(ctx, groupsConf)
	if err != nil {
		log.Fatal(err)
	}
	groups := r.GetValue()
	for _, g1 := range groups {
		fmt.Printf("g: %+v\n", g1)
		id := *g1.GetId()
		fmt.Printf("id => %s\n", id)
		displayName := *g1.GetDisplayName()
		fmt.Printf("displayName => %s\n", displayName)
		fmt.Printf("securityIdentifier => %s\n", *g1.GetSecurityIdentifier())
		fmt.Printf("mail => %s\n", *g1.GetMail())
		fmt.Println("--------------------------------")

		filter := fmt.Sprintf("startswith(displayName,'%s')", strings.Split(displayName, " ")[0])
		g2Conf := &graphgroups.GroupsRequestBuilderGetRequestConfiguration{
			QueryParameters: &graphgroups.GroupsRequestBuilderGetQueryParameters{
				Select: selects,
				Filter: &filter,
			},
		}

		g2r, err := client.Groups().Get(ctx, g2Conf)
		if err != nil {
			log.Fatal(err)
		}
		g2 := g2r.GetValue()[0]
		fmt.Println("searched g2:")
		fmt.Println(" - id:", *g2.GetId())
		fmt.Println(" - displayName:", *g2.GetDisplayName())

		fmt.Println("================================")
	}
}
