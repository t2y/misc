package main

import (
	"fmt"
	"reflect"
)

type C struct {
	value string
}

type B struct {
	values []int
	c      C
}

type A struct {
	value  bool
	values interface{}
	m      map[string]string
	b      B
}

func getValue(v reflect.Value, name string) any {
	rv := reflect.Indirect(v)
	switch rv.Kind() {
	case reflect.Struct:

	case reflect.Slice:

	default:
		fmt.Println(rv)
	}

	return nil
}

func main() {
	a := &A{
		value:  true,
		values: []string{"x", "y"},
		m: map[string]string{
			"z": "zzz",
			"o": "ooo",
		},
		b: B{
			values: []int{10, 20},
			c: C{
				value: "test-ccc",
			},
		},
	}
	fmt.Printf("a => %+v\n", a)

	for _, v := range a.values.([]string) {
		fmt.Println(v)
	}

	rvalues := reflect.ValueOf(a.values)
	var stringSliceType = reflect.TypeOf(([]string)(nil))
	switch rvalues.Kind() {
	case reflect.Bool:
		fmt.Println("bool!!!")
	case reflect.Slice:
		fmt.Println("slice!!!")
		fmt.Printf("%T %s\n", rvalues.Type(), rvalues.Type())
		switch rvalues.Type() {
		case stringSliceType:
			fmt.Println("[]string!")
		default:
			fmt.Println("default")
		}

	default:
		fmt.Println("default!!!")
	}

	// reflection
	// struct a
	ra := reflect.Indirect(reflect.ValueOf(a))
	fmt.Printf("kind: %+v\n", ra.Kind())
	fmt.Println(ra.FieldByName("value"))
	values := ra.FieldByName("values")
	fmt.Printf("kind: %+v\n", values.Kind())
	elemValues := values.Elem()
	fmt.Printf("%+v\n", elemValues)
	if elemValues.Kind() == reflect.Slice {
		fmt.Printf("%+v\n", values.Elem().Index(0))
		fmt.Printf("%+v\n", values.Elem().Index(1))
	}
	m := ra.FieldByName("m")
	fmt.Printf("kind: %+v\n", m.Kind())
	fmt.Println(m.MapIndex(reflect.ValueOf("z")))
	fmt.Println(m.MapIndex(reflect.ValueOf("o")))

	// struct b
	rb := reflect.Indirect(ra.FieldByName("b"))
	fmt.Printf("%v\n", rb.Type())

	bValues := ra.FieldByName("b").FieldByName("values")
	fmt.Printf("%T, %v, %+v\n", bValues, bValues.Kind(), bValues)
	if bValues.Kind() == reflect.Slice {
		fmt.Println("0:", bValues.Index(0))
		fmt.Println("1:", bValues.Index(1))
	}

	// struct c
	fmt.Println("c:", ra.FieldByName("b").FieldByName("c").FieldByName("value"))
}
