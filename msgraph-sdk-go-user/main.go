package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/policy"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
	"github.com/microsoftgraph/msgraph-sdk-go/models/odataerrors"
	graphusers "github.com/microsoftgraph/msgraph-sdk-go/users"
)

var (
	clientID     = os.Getenv("CLIENT_ID")
	clientSecret = os.Getenv("CLIENT_SECRET")
	tenantID     = os.Getenv("TENANT_ID")

	defaultScopes = []string{
		"https://graph.microsoft.com/.default",
	}
)

func main() {
	options := &azidentity.ClientSecretCredentialOptions{
		ClientOptions: azcore.ClientOptions{
			Retry: policy.RetryOptions{
				MaxRetries:    3,
				TryTimeout:    10 * time.Second,
				RetryDelay:    20 * time.Second,
				MaxRetryDelay: 80 * time.Second,
			},
		},
	}
	cred, err := azidentity.NewClientSecretCredential(
		tenantID, clientID, clientSecret, options,
	)
	client, err := msgraphsdk.NewGraphServiceClientWithCredentials(cred, defaultScopes)
	if err != nil {
		log.Fatal(err)
	}
	ctx := context.Background()
	selects := []string{
		"id",
		"displayName",
		"securityIdentifier",
		"userPrincipalName",
	}
	usersConf := &graphusers.UsersRequestBuilderGetRequestConfiguration{
		QueryParameters: &graphusers.UsersRequestBuilderGetQueryParameters{
			Select: selects,
		},
	}
	r, err := client.Users().Get(ctx, usersConf)
	if err != nil {
		log.Fatal(err)
	}
	users := r.GetValue()
	for _, u1 := range users {
		fmt.Printf("u: %+v\n", u1)
		id := *u1.GetId()
		fmt.Printf("id => %s\n", id)
		fmt.Printf("displayName => %s\n", *u1.GetDisplayName())
		userPrincipalName := *u1.GetUserPrincipalName()
		fmt.Printf("userPrincipalName => %s\n", userPrincipalName)
		fmt.Printf("securityIdentifier => %s\n", *u1.GetSecurityIdentifier())
		for _, v := range u1.GetAssignedLicenses() {
			fmt.Println("skuId", v.GetSkuId())
		}
		fmt.Println("--------------------------------")

		u2Conf := &graphusers.UserItemRequestBuilderGetRequestConfiguration{
			QueryParameters: &graphusers.UserItemRequestBuilderGetQueryParameters{
				Select: selects,
			},
		}
		u2, err := client.Users().ByUserId(userPrincipalName).Get(ctx, u2Conf)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("searched u2:")
		fmt.Println(" - id:", *u2.GetId())
		fmt.Println(" - userPrincipalName:", *u2.GetUserPrincipalName())

		u3, err := client.Users().ByUserId(id).Get(ctx, u2Conf)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("searched u3:")
		fmt.Println(" - id:", *u3.GetId())
		fmt.Println(" - userPrincipalName:", *u3.GetUserPrincipalName())

		_, err = client.Users().ByUserId("not-found-user").Get(ctx, u2Conf)
		if err != nil {
			fmt.Printf("%T\n", err)
			if e, ok := err.(*odataerrors.ODataError); ok {
				fmt.Printf("%+v\n", e.ApiError)
				fmt.Printf("%+v\n", *e.ResponseHeaders)
				mainErrorable := e.GetErrorEscaped()
				fmt.Printf("%+v\n", mainErrorable)
				fmt.Printf("code => %+v\n", *mainErrorable.GetCode())
			}
		}
		fmt.Println("================================")
	}
}
