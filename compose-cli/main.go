package main

import (
	"context"
	"log"

	"github.com/testcontainers/testcontainers-go/modules/compose"
)

func main() {
	path := "./docker-compose.yml"
	identifier := compose.StackIdentifier("mycompose")
	c, err := compose.NewDockerComposeWith(
		compose.WithStackFiles(path),
		identifier,
	)
	if err != nil {
		log.Fatalf("failed to new compose: %v", err)
	}
	ctx := context.Background()
	if err := c.Up(ctx); err != nil {
		log.Fatalf("failed to compose up: %v", err)
	}
	/*
		if err := c.Down(ctx); err != nil {
			log.Fatalf("failed to compose down: %v", err)
		}
	*/
}
