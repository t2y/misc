package main

import (
	"fmt"

	"example.com/auth"
)

func main() {
	s := "mypassword"
	p, _ := auth.NewPassword(s)
	fmt.Printf("%s\n", p)
	fmt.Printf("%v\n", p)
	fmt.Printf("%v\n", p.Get())
	fmt.Println("auth?", p.Authenticate(s))
	fmt.Println("auth?", p.Authenticate("passwd"))
}
