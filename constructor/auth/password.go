package auth

import (
	"golang.org/x/crypto/bcrypt"
)

type Password interface {
	Authenticate(s string) error
	Get() []byte
	String() string
}

type passwd struct {
	hashed []byte
}

func (p *passwd) Authenticate(s string) error {
	return bcrypt.CompareHashAndPassword(p.hashed, []byte(s))
}

func (p *passwd) Get() []byte {
	return p.hashed
}

func (p *passwd) String() string {
	return string(p.hashed)
}

func NewPassword(s string) (Password, error) {
	h, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return &passwd{hashed: h}, nil
}
