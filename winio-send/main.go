//go:build windows
// +build windows

package main

import (
	"fmt"
	"time"

	"github.com/Microsoft/go-winio"
)

const pipeName = `\\.\pipe\my-pipe`

func send() {
	timeout := 3 * time.Second
	conn, err := winio.DialPipe(pipeName, &timeout)
	if err != nil {
		fmt.Println("DialPipe error: ", err)
		return
	}

	num, err := conn.Write([]byte("hello\n"))
	if err != nil {
		fmt.Println("Write error: ", err)
		return
	}
	fmt.Printf("write bytes: %d\n", num)

	conn.Close()
}

func main() {
	send()
}
