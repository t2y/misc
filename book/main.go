package main

import (
	"db"
	"fmt"
)

// read系のGetByIDのみが使える
type BookFetcher interface {
	GetByID(string) *db.Book
}

func main() {
	fetcher := db.NewBookDBClient()
	bookID := "12345"

	book := SearchBook(fetcher, bookID)
	fmt.Println(book)
	AddBook(fetcher, *book)
}

// 引数をInterfaceにすることで、不必要な関数に依存しなくなる
func SearchBook(fetcher BookFetcher, bookID string) *db.Book {
	return fetcher.GetByID(bookID)
}

type BookAdder interface {
	Add(db.Book)
}

func AddBook(adder BookAdder, book db.Book) {
	adder.Add(book)
}
