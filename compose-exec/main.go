package main

import (
	"flag"
	"fmt"
	"os/exec"
)

func main() {
	flag.Parse()
	args := flag.Args()
	composeArgs := []string{
		"compose",
		"-f",
		"./docker-compose.yml",
	}
	composeArgs = append(composeArgs, args...)
	cmd := exec.Command("docker", composeArgs...)
	output, err := cmd.CombinedOutput()
	fmt.Println(string(output))
	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
