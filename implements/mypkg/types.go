package mypkg

type MyInterface interface {
	X() error
	Y(string) (int, error)
}
