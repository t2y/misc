package main

import (
	"fmt"
	"mypkg"
)

var _ mypkg.MyInterface = (*MyImplements)(nil)

type MyImplements struct {
	//	mypkg.MyInterface
}

func (o *MyImplements) X() error {
	return nil
}

/*
func (o *MyImplements) Y(value string) (int, error) {
	return 0, nil
}
*/

func call(o mypkg.MyInterface) {
}

/*
func call2(o1 mypkg.MyInterface, o2 mypkg.MyInterface) {
	fmt.Printf("o1 Type: %T, Value: %v\n", o1, o1)
	fmt.Printf("o2 Type: %T, Value: %v\n", o2, o2)
	fmt.Println("==============================")
	fmt.Println("o1 == nil compare:", o1 == nil)
	fmt.Println("o1 == (*MyImplements)(nil) compare:", o1 == (*MyImplements)(nil))
	fmt.Println("==============================")
	fmt.Println("o2 == nil compare:", o2 == nil)
	fmt.Println("o2 == (*MyImplements)(nil) compare:", o2 == (*MyImplements)(nil))
}
*/

func main() {
	o := &MyImplements{}
	fmt.Println("o:", o)
	// インターフェースを受け取る関数呼び出し
	//call(o)

	// interface 型は型と値の参照を属性としてもつオブジェクト
	// nil は型をもつことができる
	//i := (*MyImplements)(nil)
	//call2(nil, i)
}
