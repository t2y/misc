package main

import (
	"errors"
	"fmt"
	"log/slog"
	"runtime/debug"
)

func myFunc2() error {
	panic(errors.New("something error"))
}

func myFunc1() (i int, err error) {
	defer func() {
		if e := recover(); e != nil {
			slog.Error("recoverd", "err", e, "stack", string(debug.Stack()))
			err = fmt.Errorf("converted: %s", e)

			i = 2
		}
	}()
	return 3, myFunc2()
}

func main() {
	i, err := myFunc1()
	if err != nil {
		slog.Error("main gets an error", "err", err)
	}
	fmt.Println("i =>", i)
}
