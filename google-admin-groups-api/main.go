package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"golang.org/x/oauth2/google"
	admin "google.golang.org/api/admin/directory/v1"
	"google.golang.org/api/googleapi"
	"google.golang.org/api/option"
)

var (
	clientEmail = os.Getenv("CLIENT_EMAIL")
	privateKey  = strings.ReplaceAll(os.Getenv("PRIVATE_KEY"), "\\n", "\n")
	subject     = os.Getenv("SUBJECT")

	primaryEmail = os.Getenv("PRIMARY_EMAIL")
)

func newService() (*admin.Service, error) {
	ctx := context.Background()
	credentials, err := json.Marshal(map[string]string{
		"client_email": clientEmail,
		"private_key":  privateKey,
		"type":         "service_account",
	})
	if err != nil {
		return nil, err
	}
	config, err := google.JWTConfigFromJSON(
		credentials,
		admin.AdminDirectoryUserScope,
		admin.AdminDirectoryGroupScope,
	)
	if err != nil {
		return nil, fmt.Errorf("create jwt config error: %w", err)
	}
	config.Subject = subject
	ts := config.TokenSource(ctx)
	srv, err := admin.NewService(ctx, option.WithTokenSource(ts))
	if err != nil {
		return nil, fmt.Errorf("create new service error: %w", err)
	}
	return srv, nil
}

func insertGroup(srv *admin.Service, pEmail string) error {
	names := strings.Split(strings.Split(pEmail, "@")[0], "-")
	ag := admin.Group{
		Email: pEmail,
		Name:  names[0],
	}
	created, err := srv.Groups.Insert(&ag).Do()
	if err != nil {
		if ge, ok := err.(*googleapi.Error); ok {
			fmt.Printf("%+v\n", ge)
		}
		return err
	}
	fmt.Printf("created: %+v\n", created)
	return nil
}

func listGroups(srv *admin.Service, domain string) error {
	res, err := srv.Groups.List().Domain(domain).
		MaxResults(5).OrderBy("email").Do()
	if err != nil {
		if ge, ok := err.(*googleapi.Error); ok {
			fmt.Printf("%+v\n", ge)
		}
		return err
	}
	if len(res.Groups) == 0 {
		fmt.Println("no groups")
	}
	for _, u := range res.Groups {
		fmt.Printf("%+v\n", u.Name)
	}
	return nil
}

func insertAliasAndDelete(srv *admin.Service, pEmail string) error {
	var err error
	var alias *admin.Alias
	aliasParam := &admin.Alias{
		Alias: "my-alias1@" + strings.Split(pEmail, "@")[1],
	}
	for i := 0; i < 200; i++ {
		alias, err = srv.Groups.Aliases.Insert(pEmail, aliasParam).Do()
		if err != nil {
			if ge, ok := err.(*googleapi.Error); ok {
				fmt.Printf("i: %+v\n", i)
				fmt.Printf("Code: %+v\n", ge.Code)
				fmt.Printf("Message: %+v\n", ge.Message)
				fmt.Printf("Details: %+v\n", ge.Details)
			} else {
				log.Print(err.Error())
			}
		}
		if alias != nil {
			fmt.Printf("insert-aliased: %+v\n", alias)
			break
		}
		<-time.After(100 * time.Millisecond)
	}

	if alias != nil {
		if err := srv.Groups.Delete(pEmail).Do(); err != nil {
			if ge, ok := err.(*googleapi.Error); ok {
				fmt.Printf("%+v\n", ge)
			}
			log.Fatal(err)
		}
		fmt.Println("deleted after insert alias")
	}
	return nil
}

func main() {
	srv, err := newService()
	if err != nil {
		log.Fatal(err)
	}
	if err := insertGroup(srv, primaryEmail); err != nil {
		log.Fatal(err)
	}
	if err := insertAliasAndDelete(srv, primaryEmail); err != nil {
		log.Fatal(err)
	}
	if err := listGroups(srv, strings.Split(primaryEmail, "@")[1]); err != nil {
		log.Fatal(err)
	}
}
